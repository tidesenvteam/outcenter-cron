FROM fedora:latest

LABEL maintainer.name="Luiz Fernando Pereira"
LABEL maintainer.email="luiz.fernando@outcenter.com.br"
LABEL maintainer.company="Outcenter"

RUN dnf -y install \
    cronie cronie-anacron \
    dnf-plugins-core \
    && dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo \
    && dnf install docker-ce-cli -y --releasever=28 \
    && dnf clean all

COPY scripts/run-container /bin/

CMD ["crond", "-np"]